import unittest
from soundops import soundadd
from mysound import Sound


class TestSoundAdd(unittest.TestCase):
    def test_soundadd_same_length(self):
        # Prueba la suma de dos sonidos con la misma longitud
        s1 = Sound(1.0)
        s1.buffer = [1, 2, 3]

        s2 = Sound(1.0)
        s2.buffer = [4, 5, 6]

        result = soundadd(s1, s2)

        self.assertEqual(result.buffer, [5, 7, 9])
        self.assertEqual(result.duration, 1.0)

    def test_soundadd_different_length(self):
        # Prueba la suma de dos sonidos con longitudes diferentes
        s1 = Sound(1.0)
        s1.buffer = [1, 2, 3]

        s2 = Sound(0.5)
        s2.buffer = [4, 5]

        result = soundadd(s1, s2)

        self.assertEqual(result.buffer, [5, 7, 3])
        self.assertEqual(result.duration, 1.0)

    def test_soundadd_empty_sound(self):
        # Prueba la suma de un sonido vacío con uno no vacío
        s1 = Sound(1.0)
        s1.buffer = []

        s2 = Sound(1.0)
        s2.buffer = [4, 5, 6]

        result = soundadd(s1, s2)

        self.assertEqual(result.buffer, s2.buffer)
        self.assertEqual(result.duration, 1.0)


if __name__ == '__main__':
    unittest.main()
