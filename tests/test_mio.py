import unittest
from mysound import Sound


class TestMySoundMethods(unittest.TestCase):

    def test_duration_and_samples_calculation(self):
        # Crear una instancia 'Sound'
        sound = Sound(2.5)

        # Comprobar que la duración es correcta
        self.assertEqual(sound.duration, 2.5)
        # Comprobar que el número de muestras es correcto
        expected_samples = int(sound.samples_second * sound.duration)
        self.assertEqual(sound.nsamples, expected_samples)


if __name__ == '__main__':
    unittest.main()
