import unittest
import math
from mysoundsin import SoundSin


class TestSoundSin(unittest.TestCase):

    def test_sound_creation(self):
        duration = 1.0
        frequency = 440  # A4
        amplitude = 0.5
        sound = SoundSin(duration, frequency, amplitude)

        self.assertEqual(sound.duration, duration)
        self.assertEqual(sound.nsamples, int(sound.samples_second * duration))
        max_amp = sound.max_amplitude
        self.assertTrue(
            all(-max_amp <= sample <= max_amp
                for sample in sound.buffer))

    def test_sound_generation(self):
        duration = 0.5
        frequency = 880  # A5
        amplitude = 0.8
        sound = SoundSin(duration, frequency, amplitude)

        expected_samples = [
            int(
                amplitude * sound.max_amplitude * math.sin(
                    2 * math.pi * frequency * t
                )
            )
            for t in range(sound.nsamples)
        ]

        self.assertEqual(sound.buffer, expected_samples)

    def test_inherited_method(self):
        duration = 0.3
        frequency = 220  # A3
        amplitude = 0.6
        sound = SoundSin(duration, frequency, amplitude)

        bars = sound.bars()
        self.assertIsInstance(bars, str)
        self.assertTrue(len(bars) > 0)


if __name__ == '__main__':
    unittest.main()
