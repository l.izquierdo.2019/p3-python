import math
from mysound import Sound


class SoundSin(Sound):
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)
        self.sin(frequency, amplitude)
