from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:

    min_samples = min(len(s1.buffer), len(s2.buffer))
    result = Sound(max(s1.duration, s2.duration))

    for i in range(min_samples):
        result.buffer[i] = s1.buffer[i] + s2.buffer[i]

    if len(s1.buffer) > len(s2.buffer):
        result.buffer[min_samples:] = s1.buffer[min_samples:]
    else:
        result.buffer[min_samples:] = s2.buffer[min_samples:]

    return result
